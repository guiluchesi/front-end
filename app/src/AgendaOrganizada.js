import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import Title from './components/Title'
import Button from './components/Button'
import P from './components/Paragraph'

import agendaIcon from './assets/img/agenda-completa.png'

const ButtonGroup = styled.div`
  width: 500px;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
`

const AgendaOrganizada = () => (
  <React.Fragment>
    <img src={agendaIcon} alt="Agenda Organizada" />
    <Title big>Sua agenda <br /> está organizada</Title>
    <P>
      Nessa etapa iremos apresentar o nosso organizador de milestones. Nessa etapa o usuário irá setar objetivos para realizar durante o processo de aprendizagem da Kolab. A Kolab, por sua vez, irá ajudar no que puder o usuário a completar esses objetivos de forma agradável e funcional.
    </P>
    <ButtonGroup>
      <Link to="/"><Button ghost>Ver Depois</Button></Link>
      <Link to="/trilhas-mes"><Button>Ajustar meus objetivos</Button></Link>
    </ButtonGroup>
  </React.Fragment>
)

export default AgendaOrganizada
