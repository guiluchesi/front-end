import React, { PureComponent } from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import store from './redux/store'

import Card from './components/Card'
import Introducao from './Introducao'
import Questionario from './Questionario'
import PerfilCompleto from './PerfilCompleto'
import RotinaSemana from './RotinaSemana'
import RotinaHora from './RotinaHora'
import AgendaOrganizada from './AgendaOrganizada'
import TrilhasMes from './TrilhasMes'
import CapitulosSemana from './CapitulosSemana'
import ObjetivosDeterminados from './ObjetivosDeterminados'

class App extends PureComponent {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <main>
            <Card>
              <Switch>
                <Route exact path="/" component={Introducao} />
                <Route exact path="/questionario" component={Questionario} />
                <Route exact path="/perfil-completo" component={PerfilCompleto} />
                <Route exact path="/rotina-semana" component={RotinaSemana} />
                <Route exact path="/rotina-hora" component={RotinaHora} />
                <Route exact path="/agenda-organizada" component={AgendaOrganizada} />
                <Route exact path="/trilhas-mes" component={TrilhasMes} />
                <Route exact path="/capitulos-semana" component={CapitulosSemana} />
                <Route exact path="/objetivos-determinados" component={ObjetivosDeterminados} />
              </Switch>
            </Card>
          </main>
        </Router>
      </Provider>
    )
  }
}

export default App;
