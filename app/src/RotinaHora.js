import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import store from './redux/store'
import { connect } from 'react-redux'
import { rotinaHorasAction } from './redux/reducers'

import Label from './components/Label'
import Title from './components/Title'
import P from './components/Paragraph'

import Button from './components/Button'

const HoursWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 10px 0;
`

const DateInput = styled.input`
  background-color: #7750fc;
  background-color: var(--roxo);
  color: #fff;
  border: none;
  text-align: center;
  padding: 5px;
  border-radius: 4px;
  font-family: 'Gotham Rounded Bold','sans-serif';
  font-size: 1rem;
`

const DateDivider = styled.p`
  margin: 0 15px;
  font-family: 'Gotham Rounded Bold','sans-serif';
  font-size: 1.2rem;
`

class RotinaHora extends React.Component {
  atualizarHora = ({ target }) => {
    const { value, name } = target
    const novaHora = {}
    novaHora[name] = value
    console.log( name, value )
    store.dispatch(
      rotinaHorasAction(novaHora)
    )
  }

  adicionarHoras = ( horasMarcadas ) => {
    const novoBlocoDeHora = horasMarcadas.push({
      inicio: '',
      fim: '',
    })

    store.dispatch(
      rotinaHorasAction(novoBlocoDeHora)
    )
  }

  render() {
    const { horasMarcadas } = this.props
    console.log( horasMarcadas )
    return (
      <React.Fragment>
        <Label>Etapa 2</Label>
        <Title>Sua Rotina</Title>
        <P big>Quais os melhores horarários para você estudar?</P>
        <br />
        {
          horasMarcadas &&
          horasMarcadas.map( (hora, key) => (
            <HoursWrapper key={key}>
              <DateInput name="inicio" type="time" value={hora.inicio} onChange={this.atualizarHora} />
              <DateDivider>às</DateDivider>
              <DateInput name="fim" type="time" value={hora.fim} onChange={this.atualizarHora} />
            </HoursWrapper>
          ) )
        }
        <Button ghost rounded onClick={ () => this.adicionarHoras(horasMarcadas) }>Acrescentar mais horários</Button>
        <br />
        <Link to="/agenda-organizada"><Button>Continuar</Button></Link>
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({ rotinaHorasReducer }) => ({
  horasMarcadas: rotinaHorasReducer.horas
})

export default connect(mapStateToProps)(RotinaHora)
