import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import store from './redux/store'
import { connect } from 'react-redux'
import { rotinaSemanaAction } from './redux/reducers'

import Label from './components/Label'
import Title from './components/Title'
import P from './components/Paragraph'
import PanelTab from './components/PanelTab'

import Button from './components/Button'

const DaysWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 40px 0;
`

const valueFromNodeList = ( nodeList ) => Array
  .from( nodeList )
  .map( item => item.value )

const diasDaSemana = [
  {
    value: 'Domingo',
    label: 'D',
  },
  {
    value: 'Segunda',
    label: 'S',
  },
  {
    value: 'Terça',
    label: 'T',
  },
  {
    value: 'Quarta',
    label: 'Q',
  },
  {
    value: 'Quinta',
    label: 'Q',
  },
  {
    value: 'Sexta',
    label: 'S',
  },
  {
    value: 'Sábado',
    label: 'S',
  },
]

class RotinaSemana extends React.Component {
  rotinaUpdate = ({ target }) => {
    const diasSelecionados = target.parentElement.parentElement.querySelectorAll('input:checked')
    const melhoresDias = valueFromNodeList( diasSelecionados )

    store.dispatch(
      rotinaSemanaAction({
        semana: melhoresDias
      })
    )
  }

  render() {
    const { diasSelecionados } = this.props
    return (
      <React.Fragment>
        <Label>Etapa 2</Label>
        <Title>Sua Rotina</Title>
        <P big>Quais são os melhores dias para você estudar?</P>
        <DaysWrapper onChange={ this.rotinaUpdate }>
          {
            diasDaSemana &&
            diasDaSemana.map( dia => (<PanelTab checked={diasSelecionados.includes(dia.value)} profissao={dia.value} type="checkbox" ghost round>{dia.label}</PanelTab>) )
          }
        </DaysWrapper>
        <Link to="/rotina-hora"><Button>Continuar</Button></Link>
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({ rotinaSemanaReducer }) => ({
  diasSelecionados: rotinaSemanaReducer.semana
});

export default connect(mapStateToProps)(RotinaSemana)
