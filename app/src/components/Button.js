import React from 'react'
import styled, { css } from 'styled-components'

const StyledButton = styled.button`
  padding: 10px 30px;
  background-color: #7750fc;
  background-color: var(--roxo);
  color: #fff;
  border: none;
  border-radius: 4px;
  font-size: 0.8rem;
  font-family: 'Gotham Rounded Book';
  letter-spacing: 1px;
  cursor: pointer;
  margin-top: 20px;
  min-width: 230px;

  &:hover {
    box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.2);
  }

  ${
    props => props.ghost &&
    css`
      background-color: transparent;
      border: 1px solid var(--roxo);
      color: var(--roxo);
    `
  }

  ${
    props => props.rounded &&
    css`
      border-radius: 20px;
      opacity: 0.4;
      margin-bottom: 20px;
      min-width: unset;
      font-size: 0.6rem;
      padding: 10px 15px;
      transition: background-color .2s, color .2s, opacity .2s;
      will-change: background-color, color, opacity;

      &:hover {
        background-color: #7750fc;
        background-color: var(--roxo);
        color: #fff;
        opacity: 1;
      }
    `
  }
`

const Button = ({ children, ghost, rounded, onClick }) => (
  <StyledButton
    onClick={onClick}
    ghost={ghost}
    rounded={rounded}
  >
    {children}
  </StyledButton>
)

export default Button
